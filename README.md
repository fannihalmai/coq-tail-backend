# To run

1. npm install

2. Create a file named ".env" in the root of your project

3. Specify the port ("PORT=7000" for example)

4. On another line, specify the connection string to your mongoDB instance (MONGODB_URI=mongodb+srv://\<username>:\<password>@cluster0.hjz17.mongodb.net/<database_name>?retryWrites=true&w=majority)

5. npm run dev
