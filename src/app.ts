import express from "express";
import cors from "cors";
import helmet from "helmet";
import mongoose from "mongoose";
import { useExpressServer } from "routing-controllers";

import { CoreController } from "./controller/core";
import { UsersController } from "./controller/users.ctl";

import { MONGODB_URI } from "./util/secrets";

const app = express();

/**
 *  App Configuration
 */
app.use(helmet());
app.use(cors());
app.use(express.json());

const controllers: any = [CoreController, UsersController];

useExpressServer(app, {
    controllers,
    validation: {
        whitelist: true,
        forbidNonWhitelisted: true,
        skipMissingProperties: false,
        forbidUnknownValues: true
    },
    defaults: {
        // with this option, null will return 404 by default
        nullResultCode: 404,
        // with this option, void or Promise<void> will return 200 by default
        undefinedResultCode: 200,
        paramOptions: {
            // with this option, argument will be required by default
            required: true
        }
    }
});

/**
 *  Connect to DB
 */
 const mongoUrl= MONGODB_URI;
 
 mongoose.connect(mongoUrl!).then(
     () => { /** ready to use. The `mongoose.connect()` promise resolves to undefined. */ },
 ).catch(err => {
     console.log(`MongoDB connection error. Please make sure MongoDB is running. ${err}`);
     process.exit(1);
 });

 export default app;