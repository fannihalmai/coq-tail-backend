import * as os from "os";
import { Controller, Get } from "routing-controllers";

@Controller()
export class CoreController {
    private uptime() {
        const secNum = process.uptime();
        const hours = Math.floor(secNum / 3600);
        const mins = Math.floor((secNum - hours * 3600) / 60);
        const secs = secNum - hours * 3600 - mins * 60;

        const time = (hours < 10 ? "0" + hours : hours) + ":" + (mins < 10 ? "0" + mins : mins) + ":" + (secs < 10 ? "0" + secs : secs);
        return time;
    }

    // default /about service
    @Get("/about")
    about() {
        return {
            uptime: this.uptime(),
            nodejs: process.version,
            memoryUsage: Math.round(process.memoryUsage().rss / 1024 / 1024) + "M",
            system: {
                loadavg: os.loadavg(),
                freeMemory: Math.round(os.freemem() / 1024 / 1024) + "M",
                hostname: os.hostname()
            }
        };
    }
}
