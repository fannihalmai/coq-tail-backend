import bcrypt from "bcrypt";
import { Get, Post, Body, JsonController, Put, Delete, NotFoundError, UnauthorizedError, Param } from "routing-controllers";
import { IsEmail, IsString, IsNotEmpty, MinLength, Allow, IsOptional, ValidateNested, IsArray } from "class-validator";
import { User } from "../model/user.mdl";
import { Type } from "class-transformer";

class ProfileBody {
    @IsString()
    @IsNotEmpty()
    name!: string;

    @IsString()
    location!: string;
}

class UserBody {
    @IsEmail()
    email!: string;

    @IsNotEmpty()
    @MinLength(8)
    password!: string;

    @Allow()
    profile!: ProfileBody;
}

class UpdateUserBody {
    @IsEmail()
    @IsOptional()
    email!: string;

    @IsOptional()
    profile!: ProfileBody;
}

class PasswordBody {
    @IsNotEmpty()
    @MinLength(8)
    password!: string;
}

class LoginBody {
    @IsEmail()
    email!: string;

    @IsNotEmpty()
    password!: string;
}

class FavoriteDrinkBody {
    @IsNotEmpty()
    name!: string;

    @IsNotEmpty()
    id!: string;

    @IsNotEmpty()
    thumb!: string;
}

class UpdateDrinksBody {
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => FavoriteDrinkBody)
    @IsOptional()
    drinks!: FavoriteDrinkBody[];
}

@JsonController("/users", { transformResponse: false })
export class UsersController {
    @Get()
    async list() {
        return await User.find({}).exec();
    }

    @Post()
    async create(@Body() body: UserBody) {
        const dbDoc = new User({
            ...body
        });
        await dbDoc.save();
        return dbDoc;
    }

    @Post("/login")
    async login(@Body() body: LoginBody) {
        const dbDoc = await User.findOne({ email: body.email });

        if (!dbDoc) throw new NotFoundError(`User does not exist`);

        const loginStatus = {
            success: false,
            msg: "Login Failed :(",
            user: {}
        };

        if (bcrypt.compareSync(body.password, dbDoc.password)) {
            loginStatus.success = true;
            loginStatus.msg = "Login Successful!";
            loginStatus.user = dbDoc;
        }

        return loginStatus;
    }

    @Put("/profile/:id([a-f0-9]{24})")
    async update(@Param("id") id: string, @Body() body: UpdateUserBody) {
        const dbDoc = await User.findById(id);

        if (!dbDoc) throw new NotFoundError(`User ${id} not exists`);

        dbDoc.set(body);

        await dbDoc.save();
        return dbDoc;
    }

    @Put("/password/:id([a-f0-9]{24})")
    async updatePassword(@Param("id") id: string, @Body() body: PasswordBody) {
        const dbDoc = await User.findById(id);

        if (!dbDoc) throw new NotFoundError(`User ${id} not exists`);
        
        dbDoc.password = body.password;

        await dbDoc.save();
        return dbDoc;
    }

    @Put("/drinks/:id([a-f0-9]{24})")
    async updateDrinks(@Param("id") id: string, @Body() body: UpdateDrinksBody) {
        const dbDoc = await User.findById(id);

        if (!dbDoc) throw new NotFoundError(`User ${id} not exists`);

        dbDoc.drinks = body.drinks;

        await dbDoc.save();
        return dbDoc;
    }

    @Delete("/:id([a-f0-9]{24})")
    async delete(@Param("id") id: string) {
        const dbDoc = await User.findById(id);
        if (!dbDoc) throw new NotFoundError(`User ${id} not exists`);
        await dbDoc.remove();
    }
}