/**
 * Model for DB Collection
 */

 import { Schema, Document, model, CallbackError } from "mongoose";
 import bcrypt from "bcrypt";

 const schemaDrink = new Schema(
    {
        name: { type: String },
        id: { type: String },
        thumb: { type: String }
    },
    { _id: false }
);

 const UserSchema: Schema = new Schema(
     {
        email: { type: String, unique: true },
        password: String,
    
        profile: {
            name: String,
            location: String
        },

        drinks: [schemaDrink]
     },
     { 
        timestamps: true,
        // convert Schema Document to plain object before JSON transformation
        toJSON: {
            transform: (doc: Document, ret: any) => {
                ret.id = ret._id.toString();
                delete ret._id;
                delete ret.__v;
                delete ret.password;
            }
        }
     }
 );

 // We need to hash the password before storing
 UserSchema.pre("save", function save(next) {
    const user = this as IUser;
    if ( !user.isModified("password")) { return next(); }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) { return next(err); }
        bcrypt.hash(user.password, salt, function(err, hash){
            if (err) { return next(err); }
            user.password = hash;
            next();
        });
    });
 });
 
 // Typescript interface for Document (Model instance)
 export interface IDrink {
    name: string;
    id: string;
    thumb: string;
}

 export interface IUser extends Document {
    email: string;
    password: string;

    profile: {
        name: string;
        location: string;
    };

    drinks: IDrink[];
 }
 
 // Export the model
 export const User = model<IUser>("User", UserSchema);
 