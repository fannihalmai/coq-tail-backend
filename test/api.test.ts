import request from "supertest";
import { expect } from "chai";
import app from "../src/app";
import {
  newUser,
  payloadDrinks,
  userLogin,
  badUserLogin,
  newProfile,
} from "./config";

describe("GET /about", () => {
  it("Should return 200 OK - info on server", () => {
    return request(app).get("/about").expect(200);
  });
});

describe("GET /users", () => {
  it("Should return 200 OK - returning all users", () => {
    return request(app).get("/users").expect(200);
  });
});

let userId = "";

describe("POST /users ", () => {
  it("should return 200 - user creation", async () => {
    const res = await request(app).post("/users").send(newUser);
    expect(res.body.email).equal(newUser.email);
    expect(res.statusCode).equal(200);
    userId = res.body.id;
  });
});

describe("POST /users ", () => {
  it("should return 200 - user creation fails user exists", async () => {
    const res = await request(app).post("/users").send(newUser);
    expect(res.statusCode).equal(500);
  });
});

describe("POST /users/login", () => {
  it("should return 200 - login user", async () => {
    const res = await request(app).post("/users/login").send(userLogin);
    expect(res.body.success).equal(true);
    expect(res.body).haveOwnProperty("user");
  });
});

describe("POST /users/login", () => {
  it("should return 200 - login user success false for bad login", async () => {
    const res = await request(app).post("/users/login").send(badUserLogin);
    expect(res.body.success).equal(undefined);
  });
});

describe("PUT /users/drinks", () => {
  it("should return 200 - modify favorites of user", async () => {
    const res = await request(app)
      .put("/users/drinks/" + userId)
      .send(payloadDrinks);
    expect(res.statusCode).equal(200);
    expect(res.body).haveOwnProperty("profile");
  });
});

describe("PUT /users/drinks", () => {
  it("should return 404 - modify favorites of user", async () => {
    const res = await request(app)
      .put("/users/drinks/" + userId + "1")
      .send(payloadDrinks);
    expect(res.statusCode).equal(404);
  });
});

describe("PUT /users/password", () => {
  it("should return 200 - modify password", async () => {
    const res = await request(app)
      .put("/users/password/" + userId)
      .send({ password: "abcdefghijk" });
    expect(res.statusCode).equal(200);
    expect(res.body).haveOwnProperty("profile");
  });
});

describe("PUT /users/password", () => {
  it("should return 404 - modify password for inexistant user", async () => {
    const res = await request(app)
      .put("/users/password/" + userId + "1")
      .send({ password: "abcdefghijk" });
    expect(res.statusCode).equal(404);
  });
});

describe("PUT /users/profile", () => {
  it("should return 200 - modifications to profile data", async () => {
    const res = await request(app)
      .put("/users/profile/" + userId)
      .send(newProfile);
    expect(res.statusCode).equal(200);
    expect(res.body).haveOwnProperty("profile");
    expect(res.body).haveOwnProperty("drinks");
  });
});

describe("PUT /users/profile", () => {
  it("should return 404 - modifications to profile data for inexistant user", async () => {
    const res = await request(app)
      .put("/users/profile/" + userId + "1")
      .send(newProfile);
    expect(res.statusCode).equal(404);
  });
});

describe("POST /users/login", () => {
  it("should return 200 but success false - login user unsuccessful after changed password", async () => {
    const res = await request(app).post("/users/login").send(badUserLogin);
    expect(res.statusCode).equal(404);
  });
});

describe("DELETE /users", () => {
  it("should return 200 - deletion of user", async () => {
    const res = await request(app)
      .delete("/users/" + userId)
      .send(badUserLogin);
    expect(res.statusCode).equal(200);
  });
});

describe("DELETE /users", () => {
  it("should return 404 - user doesn't exist", async () => {
    const res = await request(app)
      .delete("/users/" + userId)
      .send();
    expect(res.statusCode).equal(404);
  });
});
