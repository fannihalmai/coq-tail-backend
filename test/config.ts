export const newUser = {
  email: "examp5l234e@gmail.com",
  password: "abcdefgh",
  profile: {
    name: "Test",
    location: "Test",
  },
};
export const payloadDrinks = {
  drinks: [
    {
      name: "coca",
      id: "123",
      thumb: "url2",
    },
    {
      name: "pepsi",
      id: "123",
      thumb: "url2",
    },
  ],
};
export let userId = "";
export const userLogin = {
  email: "examp5l234e@gmail.com",
  password: "abcdefgh",
};

export const badUserLogin = {
  email: "examp5l1111234e@gmail.com",
  password: "aaaa",
};

export const newProfile = {
  profile: {
    name: "New test",
    location: "New location",
  },
};
